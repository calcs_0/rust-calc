use std::sync::Mutex;
use std::collections::HashMap;

lazy_static!{
    pub static ref VARIABLES: Mutex<HashMap<String, f64>> = {
        let mut m = HashMap::new();
        m.insert("PI".to_string(), 3.14159265358979323846);
        Mutex::new(m)
    };
}