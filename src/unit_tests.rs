#[path = "./node.rs"]
mod node;
#[path = "./parser.rs"]
mod parser;
#[path = "./token.rs"]
mod token;
#[path = "./variables.rs"]
mod variables;
#[path = "./scanner.rs"]
mod scanner;

#[allow(unused_imports)]
use node::*;
#[allow(unused_imports)]
use parser::parse;
#[allow(unused_imports)]
use token::Token;
#[allow(unused_imports)]
use variables::VARIABLES;
#[allow(unused_imports)]
use scanner::get_tokens;

#[cfg(test)]
mod test{
    use super::*;
    
    #[test]
    fn neg() {
        let a: Box<dyn Node> = NegNode::new(NumericNode::new(14.0));
        assert_eq!(-14.0, a.calculate());
    }
    #[test]
    fn add(){
        let a: Box<dyn Node> = AddNode::new(NumericNode::new(14.0),NumericNode::new(14.0));
        assert_eq!(28.0, a.calculate());
    }
    #[test]
    fn sub(){
        let a: Box<dyn Node> = SubNode::new(NumericNode::new(14.0),NumericNode::new(14.0));
        assert_eq!(0.0, a.calculate());
    }
    #[test]
    fn mult(){
        let a: Box<dyn Node> = TimesNode::new(NumericNode::new(14.0),NumericNode::new(14.0));
        assert_eq!(196.0, a.calculate());
    }
    #[test]
    fn div(){
        let a: Box<dyn Node> = DivNode::new(NumericNode::new(14.0),NumericNode::new(14.0));
        assert_eq!(1.0, a.calculate());
    }

    #[test]
    fn parse(){
        let number = parser::parse(vec![
            token::Token::NUMBER(4.0),
            token::Token::NUMBER(2.0),
            token::Token::DIV,
            token::Token::NUMBER(1.0),
            token::Token::NUMBER(3.0),
            token::Token::DIV,
            token::Token::ADD,
            ]);
        assert_eq!(2.3333333333333335, number.calculate());
    }

    #[test]
    fn get_tokens() {
        let tokens = scanner::get_tokens("4 2/ 1 3/+");
        let tokens_test = vec![
            token::Token::NUMBER(4.0),
            token::Token::NUMBER(2.0),
            token::Token::DIV,
            token::Token::NUMBER(1.0),
            token::Token::NUMBER(3.0),
            token::Token::DIV,
            token::Token::ADD,
            ];
        let mut iter = tokens.iter();
        let mut iter_test = tokens_test.iter();

        while let Some(token) = iter.next() {
            let token_test = iter_test.next().unwrap();
            match token{
                token::Token::ADD => {
                    match token_test{
                        token::Token::ADD => {assert!(true)},
                        _ => {assert!(false)}
                    }
                },
                token::Token::SUB => {
                    match token_test{
                        token::Token::SUB => {assert!(true)},
                        _ => {assert!(false)}
                    }
                },
                token::Token::TIMES => {
                    match token_test{
                        token::Token::TIMES => {assert!(true)},
                        _ => {assert!(false)}
                    }
                },
                token::Token::DIV => {
                    match token_test{
                        token::Token::DIV => {assert!(true)},
                        _ => {assert!(false)}
                    }
                },
                token::Token::NEG => {
                    match token_test{
                        token::Token::NEG => {assert!(true)},
                        _ => {assert!(false)}
                    }
                },
                token::Token::NUMBER(n) => {
                    match token_test{
                        token::Token::NUMBER(m) => {assert_eq!(m,n)}
                        _ => {assert!(false)}
                    }
                },
                token::Token::VARIABLE(v) => {
                    match token_test{
                        token::Token::VARIABLE(u) => {assert_eq!(u,v)}
                        _ => {assert!(false)}
                    }
                }
            }
        } 
    }
}