#[macro_use]
extern crate lazy_static;
extern crate clap;

mod node;
mod parser;
mod scanner;
mod token;
mod variables;
mod unit_tests;

use clap::{Arg, App};

use parser::parse;
use scanner::get_tokens;
use node::{Node, NumericNode};
use variables::VARIABLES;

/// Retorna un objeto con los argumentos obtenidos por linea de comandos
fn args() -> clap::ArgMatches<'static> {
    return App::new("Rust Calc")
    .version(env!("CARGO_PKG_VERSION"))
    .author(env!("CARGO_PKG_AUTHORS"))
    //    .arg(Arg::with_name("file")
    //        .short("f")
    //        .long("file")
    //        .value_name("FILE")
    //        .help("A file with a list of math expressions")
    //        .takes_value(true)
    //    )
    .arg(Arg::with_name("line")
        .short("l")
        .long("line")
        .value_name("MATH_EXP")
        .help("An argument with an inline math expression")
        .takes_value(true)
    )
    .arg(Arg::with_name("variables")
        .short("V")
        .long("VAR")
        .value_name("STR=NUM")
        .min_values(1)
        .help("A list of variables that you use in any math expression")
        .takes_value(true)
    )
    .get_matches();
}

fn main() {
    let matches = args();
    let arg_variables : Vec<_> = matches.values_of("variables").unwrap_or(clap::Values::default()).collect();
    let inline_exp: &str = matches.value_of("line").unwrap_or("");
    let mut result: Box<dyn Node> = NumericNode::new(0.0);

    for v in arg_variables {
        let key_val: Vec<&str> = v.split("=").collect();
        VARIABLES.lock().unwrap().insert(key_val[0].to_string(), key_val[1].parse::<f64>().unwrap());
    }

    if inline_exp != "" {
        result = parse(get_tokens(inline_exp));
    }
    else{
        println!("No inline input");
    }

    println!("{}", result.calculate());
}