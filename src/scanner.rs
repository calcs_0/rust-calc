use std::process;
use super::VARIABLES;

use super::token;
use token::Token;

#[allow(dead_code)]
/// Retorna una lista de tokens
pub fn get_tokens(line: &str) -> Vec<Token>{
    let mut tokens: Vec<Token> = Vec::new();
    let mut iter = line.chars().peekable();
    while let Some(token) = iter.next() {
        match token{
            '+' => {tokens.push(Token::ADD)},
            '-' => {tokens.push(Token::SUB)},
            '*' => {tokens.push(Token::TIMES)},
            '/' => {tokens.push(Token::DIV)},
            '~' => {tokens.push(Token::NEG)},
            '0'..='9' =>{tokens.push(Token::NUMBER(get_number(token, &mut iter)))},
            'A'..='z' => {tokens.push(Token::VARIABLE(get_variable(token, &mut iter)))},
            ' '|'\t' => {},
            _ => {panic!("ERROR: Caracter no reconocido: {}", token)}
        }
    }
    tokens
}

fn get_number(token: char, iter: &mut std::iter::Peekable<std::str::Chars> ) -> f64{
    let mut number: String = token.to_string();
    while let Some(n) = iter.peek(){
        match n{
            '0'..='9' => {number.push(*n)},
            _ => {break}
        }
        iter.next();
    }
    number.parse::<f64>().unwrap()
}

fn get_variable(token: char, iter: &mut std::iter::Peekable<std::str::Chars> ) -> String{
    let mut variable: String = token.to_string();
    while let Some(c) = iter.peek(){
        match c{
            'A'..='z'|'0'..='9' => {variable.push(*c)},
            _ => {break}
        }
        iter.next();
    }
    if !VARIABLES.lock().unwrap().contains_key(&variable) {
        println!("ERROR: Variable {} doesn't exist", variable);
        process::exit(1);
    }
    variable
}