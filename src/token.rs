#[allow(dead_code)]
pub enum Token{
    NUMBER(f64),
    VARIABLE(String),
    ADD,
    SUB,
    DIV,
    TIMES,
    NEG
}