use std::process;
use super::VARIABLES;

/// Nodo básico
/// 
/// Se usa como Interfaz para el resto de nodos
pub trait Node{
    /// resuelve el valor de los nodos hijos recursivamente
    fn calculate(&self) -> f64;
}

/// Nodo con dos nodos hijos: left y right
/// 
/// Implementa operaciones binarias en la funcion `calculate()`
pub trait BinaryNode: Node{
    /// Obtiene el nodo hijo izquierdo perteneciente a este nodo
    fn get_left_node(&self) -> f64;
    /// Obtiene el nodo hijo derecho perteneciente a este nodo
    fn get_right_node(&self) -> f64;
}

/// Nodo con un nodo hijo
/// 
/// Implementa operaciones unarias en la funcion `calculate()`
pub trait UnaryNode: Node{
    /// Obtiene el nodo hijo perteneciente a este nodo
    fn get_node(&self) -> f64;
}


// Numeric Node
pub struct NumericNode{
    number: f64
}

impl Node for NumericNode{
    fn calculate(&self) -> f64 {
        self.number
    }
}

impl NumericNode{
    #[allow(dead_code)]
    pub fn new(number: f64) -> Box<NumericNode>{
        Box::new(NumericNode{
            number: number
        })
    }
}

// Variable Node
pub struct VariableNode{
    name: String
}

impl Node for VariableNode{
    fn calculate(&self) -> f64 {
        *VARIABLES.lock().unwrap().get(&self.name).unwrap()
    }
}

impl VariableNode{
    #[allow(dead_code)]
    pub fn new(name: String) -> Box<VariableNode>{
        Box::new(VariableNode{
            name: name
        })
    }
}


// Add Node
pub struct AddNode{
    left: Box<dyn Node>,
    right: Box<dyn Node>
}

impl Node for AddNode{
    fn calculate(&self) -> f64 {
        self.get_left_node() + self.get_right_node()
    }
}

impl BinaryNode for AddNode{
    fn get_left_node(&self) -> f64 {
        self.left.calculate()
    }
    fn get_right_node(&self) -> f64 {
        self.right.calculate()
    }
}

impl AddNode{
    #[allow(dead_code)]
    pub fn new(left: Box<dyn Node>, right: Box<dyn Node>) -> Box<AddNode> {
        Box::new(AddNode{
            left: left,
            right: right
        })
    }
}

// Sub Node
pub struct SubNode{
    left: Box<dyn Node>,
    right: Box<dyn Node>
}

impl Node for SubNode{
    fn calculate(&self) -> f64 {
        self.get_left_node() - self.get_right_node()
    }
}

impl BinaryNode for SubNode{
    fn get_left_node(&self) -> f64 {
        self.left.calculate()
    }
    fn get_right_node(&self) -> f64 {
        self.right.calculate()
    }
}

impl SubNode{
    #[allow(dead_code)]
    pub fn new(left: Box<dyn Node>, right: Box<dyn Node>) -> Box<SubNode> {
        Box::new(SubNode{
            left: left,
            right: right
        })
    }
}

// Times Node
pub struct TimesNode{
    left: Box<dyn Node>,
    right: Box<dyn Node>
}

impl Node for TimesNode{
    fn calculate(&self) -> f64 {
        self.get_left_node() * self.get_right_node()
    }
}

impl BinaryNode for TimesNode{
    fn get_left_node(&self) -> f64 {
        self.left.calculate()
    }
    fn get_right_node(&self) -> f64 {
        self.right.calculate()
    }
}

impl TimesNode{
    #[allow(dead_code)]
    pub fn new(left: Box<dyn Node>, right: Box<dyn Node>) -> Box<TimesNode> {
        Box::new(TimesNode{
            left: left,
            right: right
        })
    }
}

// Div Node
pub struct DivNode{
    left: Box<dyn Node>,
    right: Box<dyn Node>
}

impl Node for DivNode{
    fn calculate(&self) -> f64 {
        if self.get_right_node() == 0f64 {
            println!("Divide by zero is not allowed");
            process::exit(1);
        }
        self.get_left_node() / self.get_right_node()
    }
}

impl BinaryNode for DivNode{
    fn get_left_node(&self) -> f64 {
        self.left.calculate()
    }
    fn get_right_node(&self) -> f64 {
        self.right.calculate()
    }
}

impl DivNode{
    #[allow(dead_code)]
    pub fn new(left: Box<dyn Node>, right: Box<dyn Node>) -> Box<DivNode> {
        Box::new(DivNode{
            left: left,
            right: right
        })
    }
}

// Neg Node
pub struct NegNode{
    node: Box<dyn Node>
}

impl Node for NegNode{
    fn calculate(&self) -> f64 {
        -self.get_node()
    }
}

impl UnaryNode for NegNode{
    fn get_node(&self) -> f64 {
        self.node.calculate()
    }
}

impl NegNode{
    #[allow(dead_code)]
    pub fn new(node: Box<dyn Node>) -> Box<NegNode>{
        Box::new(NegNode{
            node: node
        })
    }
}