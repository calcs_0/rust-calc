use super::token;
use super::node;

use node::*;
use token::Token;

#[allow(dead_code)]
pub fn parse(tokens: Vec<Token>) -> Box<dyn Node> {
    let iter = &mut tokens.iter().rev();
    set_tree(iter)
}

fn set_tree(tokens: &mut dyn Iterator<Item = &token::Token>) -> Box<dyn Node>{
    let next = tokens.next().unwrap();
    match next{
        Token::NUMBER(n) => return NumericNode::new(*n),
        Token::VARIABLE(v) => return VariableNode::new(v.to_string()),
        Token::ADD => return set_add(tokens),
        Token::SUB => return set_sub(tokens),
        Token::TIMES => return set_times(tokens),
        Token::DIV => return set_div(tokens),
        Token::NEG => return set_neg(tokens),
    }
}

fn set_add(tokens: &mut dyn Iterator<Item = &Token>) -> Box<dyn Node>{
    let right = set_tree(tokens);
    let left = set_tree(tokens);
    AddNode::new(left, right)
}

fn set_sub(tokens: &mut dyn Iterator<Item = &Token>) -> Box<dyn Node>{
    let right = set_tree(tokens);
    let left = set_tree(tokens);
    SubNode::new(left, right)
}

fn set_times(tokens: &mut dyn Iterator<Item = &Token>) -> Box<dyn Node>{
    let right = set_tree(tokens);
    let left = set_tree(tokens);
    TimesNode::new(left, right)
}

fn set_div(tokens: &mut dyn Iterator<Item = &Token>) -> Box<dyn Node>{
    let right = set_tree(tokens);
    let left = set_tree(tokens);
    DivNode::new(left, right)
}

fn set_neg(tokens: &mut dyn Iterator<Item = &Token>) -> Box<dyn Node>{
    let node = set_tree(tokens);
    NegNode::new(node)
}